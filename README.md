# RestaurantFinder

![Android](https://img.shields.io/badge/Android-3DDC84?style=for-the-badge&logo=android&logoColor=white)
![Kotlin](https://img.shields.io/badge/kotlin-%237F52FF.svg?style=for-the-badge&logo=kotlin&logoColor=white)

RestaurantFinder est une application qui vous permet de noter les restaurants autour de vous, mais aussi de consulter les avis et notes donnés par les utilisateurs de google.

## Description

Sur la page d'accueil de l'application sont affichés les 15 premiers restaurants dans un rayon de 5km autour de la position de l'utilisateur ou de la partie visible de la carte.

En cliquant sur un restaurant on affiche la description de celui-ci contenant les reviews récupérées de l'API google maps ainsi que la note moyenne de celui-ci.
Il est aussi possible d'ajouter un commentaire et une note personelle qui pourront être retrouvés dans la liste des notes.

## Screenshots


<div align = center>
<img alt="home_page" src="App_Images/MapFragment.png" width="200" >
<img alt="restaurant_detail" src="App_Images/RestaurantDetails.png" width="200" >
<img alt="add_note" src="App_Images/AddNote.png" width="200" >
<img alt="note_detail" src="App_Images/NoteDetails.png" width="200" >
<img alt="all_note" src="App_Images/AllNotes.png" width="200" >
</div>