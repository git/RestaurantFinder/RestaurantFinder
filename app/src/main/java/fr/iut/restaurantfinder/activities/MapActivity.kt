package fr.iut.restaurantfinder.activities

import android.Manifest
import android.content.pm.PackageManager
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.commit
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import fr.iut.restaurantfinder.R
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.material.bottomnavigation.BottomNavigationView
import fr.iut.restaurantfinder.model.Restaurant
import fr.iut.restaurantfinder.model.Stub
import fr.iut.restaurantfinder.model.mapApi.RestaurantService
import fr.iut.restaurantfinder.model.mapApi.createRestaurantRetrofit
import fr.iut.restaurantfinder.view.adaptateur.RestaurantAdaptateur
import fr.iut.restaurantfinder.view.fragment.RestaurantDetailFragment
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.create
import kotlin.concurrent.thread


class MapActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.nav_host)
        Configuration.getInstance().load(this, PreferenceManager.getDefaultSharedPreferences(this));

        val navBar = findViewById<BottomNavigationView>(R.id.bottom_nav)
        val navController = findNavController(R.id.nav_host_fragment)
        navBar.setupWithNavController(navController)
    }


}
