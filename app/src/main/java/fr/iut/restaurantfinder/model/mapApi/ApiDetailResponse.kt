package fr.iut.restaurantfinder.model.mapApi

import com.squareup.moshi.Json
import fr.iut.restaurantfinder.model.RestaurantDetail

data class ApiDetailResponse(@Json(name = "status") val status: String,
                       @Json(name = "result") val result: RestaurantDetail
)