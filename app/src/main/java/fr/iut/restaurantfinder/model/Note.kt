package fr.iut.restaurantfinder.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "notes")
data class Note(
    @PrimaryKey(autoGenerate = true)
    val id: Long= 0,
    @ColumnInfo(name = "avis")
    val avis: String,
    @ColumnInfo(name = "etoile")
    val etoile: Double,
    @ColumnInfo(name = "restaurant")
    val restaurant: Restaurant)