package fr.iut.restaurantfinder.model

import com.squareup.moshi.Json
import fr.iut.restaurantfinder.model.mapApi.Geometry
import fr.iut.restaurantfinder.model.mapApi.Review

data class RestaurantDetail(@Json(name = "website") val website: String, @Json(name = "reviews") val reviews: List<Review>)
