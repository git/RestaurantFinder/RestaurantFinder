package fr.iut.restaurantfinder.model.mapApi

import com.squareup.moshi.Json

data class ApiLocation(@Json(name = "lat") val latitude: Double, @Json(name = "lng") val longitude: Double) {
}