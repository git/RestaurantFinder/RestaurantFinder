package fr.iut.restaurantfinder.model

import com.squareup.moshi.Json
import fr.iut.restaurantfinder.model.mapApi.Geometry
import fr.iut.restaurantfinder.model.mapApi.Opening_Hours

data class Restaurant(val geometry: Geometry, @Json(name = "place_id") val id: String,  @Json(name = "name")  val name: String, @Json(name = "rating") val rating: Double, @Json(name = "types") val types: List<String>, @Json(name = "vicinity") val adresse: String) {

    fun isEqual(r: Restaurant): Boolean = id == r.id
}

