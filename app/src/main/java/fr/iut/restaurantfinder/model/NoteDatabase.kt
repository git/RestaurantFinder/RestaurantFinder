package fr.iut.restaurantfinder.model

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [Note::class], version = 1)
@TypeConverters(RestaurantTypeConverter::class)
abstract class NoteDatabase : RoomDatabase(){
    abstract fun noteDao(): NoteDao

    companion object{
        private var INSTANCE : NoteDatabase? = null

        fun getInstace(context: Context)=
            INSTANCE?: synchronized(this){
                val a = Room.databaseBuilder(context, NoteDatabase::class.java,
                "note_db").build()
                INSTANCE=a
                a
            }
    }
}