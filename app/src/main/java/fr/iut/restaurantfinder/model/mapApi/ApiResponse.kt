package fr.iut.restaurantfinder.model.mapApi

import com.squareup.moshi.Json
import fr.iut.restaurantfinder.model.Restaurant

data class ApiResponse(@Json (name = "status") val status: String,
                       @Json (name = "results") val results: List<Restaurant>)
