package fr.iut.restaurantfinder.model

import androidx.room.TypeConverter
import com.google.gson.Gson

class RestaurantTypeConverter {
    @TypeConverter
    fun fromRestaurant(restaurant: Restaurant): String {
        return Gson().toJson(restaurant)
    }

    @TypeConverter
    fun toRestaurant(json: String): Restaurant {
        return Gson().fromJson(json, Restaurant::class.java)
    }
}