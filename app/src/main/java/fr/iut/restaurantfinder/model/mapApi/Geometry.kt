package fr.iut.restaurantfinder.model.mapApi

import com.squareup.moshi.Json

data class Geometry(@Json(name = "location") val location: ApiLocation) {
}