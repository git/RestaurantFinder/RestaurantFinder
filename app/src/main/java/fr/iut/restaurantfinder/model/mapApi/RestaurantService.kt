package fr.iut.restaurantfinder.model.mapApi

import android.util.Log
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import fr.iut.restaurantfinder.model.Restaurant
import okhttp3.MediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


private const val GOOGLE_API_BASE = "https://maps.googleapis.com/maps/api/"

val httpClient = OkHttpClient()


interface RestaurantService {

    @GET("place/nearbysearch/json?&radius=5000&type=restaurant&key=AIzaSyAHfLcFIIKH1WRhA6EJawjf4FMZDYGmlAU")
    fun getRestaurants(
        @Query("location", encoded = true) location: String,
    ): Call<ApiResponse>


    @GET("place/details/json?&key=AIzaSyAHfLcFIIKH1WRhA6EJawjf4FMZDYGmlAU")
    fun getRestaurantDetails(
        @Query("place_id", encoded = true) id: String,
    ): Call<ApiDetailResponse>

}

fun createRestaurantRetrofit(): Retrofit =
    Retrofit.Builder().baseUrl(GOOGLE_API_BASE)
        .addConverterFactory(
            MoshiConverterFactory.create(
                Moshi.Builder()
                    .add(KotlinJsonAdapterFactory())
                    .build()))
        .client(httpClient)
        .build()