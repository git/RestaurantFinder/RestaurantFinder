package fr.iut.restaurantfinder.model.mapApi

import com.squareup.moshi.Json

data class Review(@Json(name = "author_name") val author: String, @Json(name = "relative_time_description") val date: String, @Json(name = "text") val text: String, @Json(name = "rating") val rating: Double )