package fr.iut.restaurantfinder.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fr.iut.restaurantfinder.model.Note
import fr.iut.restaurantfinder.model.Restaurant

class RestaurantViewModel : ViewModel() {
    private val _restaurant= MutableLiveData<Restaurant>()
    val restaurant: LiveData<Restaurant> = _restaurant

    fun setRestaurant(restaurant: Restaurant) {
        _restaurant.value = restaurant
    }
}