package fr.iut.restaurantfinder.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import fr.iut.restaurantfinder.model.Note
import fr.iut.restaurantfinder.model.Restaurant

class NoteViewModel : ViewModel(){
    private val _note = MutableLiveData<Note>()
    val note: LiveData<Note> = _note

    fun setNote(note: Note) {
        _note.value = note
    }
}