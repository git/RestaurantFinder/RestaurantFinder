package fr.iut.restaurantfinder.view.adaptateur

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import fr.iut.restaurantfinder.R
import fr.iut.restaurantfinder.model.Restaurant
import fr.iut.restaurantfinder.model.mapApi.Review
import fr.iut.restaurantfinder.view.viewHolder.RestaurantViewHolder
import fr.iut.restaurantfinder.view.viewHolder.ReviewViewHolder
import fr.iut.restaurantfinder.viewmodel.RestaurantViewModel

class ReviewAdaptateur(val reviews: List<Review>) : RecyclerView.Adapter<ReviewViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewViewHolder {
        return ReviewViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.review_cellule, parent, false))
    }

    override fun onBindViewHolder(holder: ReviewViewHolder, position: Int) {
        holder.viewItem.findViewById<TextView>(R.id.author).text = reviews[position].author
        holder.viewItem.findViewById<TextView>(R.id.author_rating).text = "${reviews[position].rating} ⭐️"
        holder.viewItem.findViewById<TextView>(R.id.date).text = reviews[position].date
        holder.viewItem.findViewById<TextView>(R.id.review).text = reviews[position].text

    }

    override fun getItemCount(): Int {
        return reviews.size
    }




}