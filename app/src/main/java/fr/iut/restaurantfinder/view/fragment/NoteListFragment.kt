import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.activity.addCallback
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.iut.restaurantfinder.R
import fr.iut.restaurantfinder.model.Note
import fr.iut.restaurantfinder.model.NoteDatabase
import fr.iut.restaurantfinder.model.Restaurant
import fr.iut.restaurantfinder.view.adaptateur.RestaurantAdaptateur
import fr.iut.restaurantfinder.viewmodel.NoteViewModel
import fr.iut.restaurantfinder.viewmodel.RestaurantViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.Marker

class NoteListFragment: Fragment(R.layout.all_notes_layout) {

    private var notes: ObservableList<Note> = ObservableArrayList()

    private val sharedViewModel: NoteViewModel by activityViewModels()


    override fun onViewCreated(v: View, savedInstanceState: Bundle?) {
        super.onViewCreated(v, savedInstanceState)

        val noteDatabase = NoteDatabase.getInstace(requireContext())

        val recyc = v.findViewById<RecyclerView>(R.id.recyclerView_allnotes)
        recyc.layoutManager = LinearLayoutManager(requireContext())
        recyc.adapter = NoteAdaptateur(notes, sharedViewModel)

        GlobalScope.launch {
            notes.addAll(noteDatabase.noteDao().getAllNotes())
        }

        notes.addOnListChangedCallback(object :
            ObservableList.OnListChangedCallback<ObservableList<Note>>(){
            override fun onChanged(sender: ObservableList<Note>?) {
                val handler = Handler(Looper.getMainLooper())
                handler.post(Runnable { // Update UI code here
                    recyc.adapter = NoteAdaptateur(notes, sharedViewModel)
                })
            }

            override fun onItemRangeChanged(
                sender: ObservableList<Note>?,
                positionStart: Int,
                itemCount: Int
            ) {
                val handler = Handler(Looper.getMainLooper())
                handler.post(Runnable { // Update UI code here
                    recyc.adapter = NoteAdaptateur(notes, sharedViewModel)
                })
            }

            override fun onItemRangeInserted(
                sender: ObservableList<Note>?,
                positionStart: Int,
                itemCount: Int
            ) {
                val handler = Handler(Looper.getMainLooper())
                handler.post(Runnable { // Update UI code here
                    recyc.adapter = NoteAdaptateur(notes, sharedViewModel)
                })
            }

            override fun onItemRangeMoved(
                sender: ObservableList<Note>?,
                fromPosition: Int,
                toPosition: Int,
                itemCount: Int
            ) {
            }

            override fun onItemRangeRemoved(
                sender: ObservableList<Note>?,
                positionStart: Int,
                itemCount: Int
            ) {
            }
        })


    }
}