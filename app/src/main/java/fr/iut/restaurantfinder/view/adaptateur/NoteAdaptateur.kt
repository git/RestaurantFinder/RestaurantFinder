import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import fr.iut.restaurantfinder.R
import fr.iut.restaurantfinder.model.Note
import fr.iut.restaurantfinder.view.viewHolder.NoteViewHolder
import fr.iut.restaurantfinder.viewmodel.NoteViewModel

class NoteAdaptateur(val notes: List<Note>, val sharedViewModel: NoteViewModel) : RecyclerView.Adapter<NoteViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        return NoteViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.note_cellule, parent, false))
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        holder.viewItem.setOnClickListener {
            sharedViewModel.setNote(notes[position])
            holder.viewItem.findNavController().navigate(R.id.noteFragment)
        }
        holder.viewItem.findViewById<TextView>(R.id.restaurant_name_cellule).text = notes[position].restaurant.name
        holder.viewItem.findViewById<TextView>(R.id.user_rating).text = "${notes[position].etoile} ⭐️"
    }

    override fun getItemCount(): Int {
        return notes.size
    }




}