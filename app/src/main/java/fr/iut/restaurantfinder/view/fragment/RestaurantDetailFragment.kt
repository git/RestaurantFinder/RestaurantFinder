package fr.iut.restaurantfinder.view.fragment

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.iut.restaurantfinder.R
import fr.iut.restaurantfinder.model.Note
import fr.iut.restaurantfinder.model.Restaurant
import fr.iut.restaurantfinder.model.mapApi.ApiDetailResponse
import fr.iut.restaurantfinder.model.mapApi.ApiResponse
import fr.iut.restaurantfinder.model.mapApi.RestaurantService
import fr.iut.restaurantfinder.model.mapApi.createRestaurantRetrofit
import fr.iut.restaurantfinder.view.adaptateur.NoteImageAdaptateur
import fr.iut.restaurantfinder.view.adaptateur.RestaurantAdaptateur
import fr.iut.restaurantfinder.view.adaptateur.ReviewAdaptateur
import fr.iut.restaurantfinder.viewmodel.NoteViewModel
import fr.iut.restaurantfinder.viewmodel.RestaurantViewModel
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.overlay.Marker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.create

class RestaurantDetailFragment: Fragment(R.layout.restaurant_layout) {

    private val sharedViewModel: RestaurantViewModel by activityViewModels()
    private val service = createRestaurantRetrofit().create<RestaurantService>()


    override fun onViewCreated(v: View, savedInstanceState: Bundle?) {
        super.onViewCreated(v, savedInstanceState)
        val restaurant = sharedViewModel.restaurant.value

        val recyc = requireView().findViewById<RecyclerView>(R.id.recycler_view_review)
        recyc.layoutManager = LinearLayoutManager(requireContext())

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(R.id.mapFragment)
        }

        if (restaurant != null) {
            Log.d("TAG", "${restaurant.id}" )
            getRestaurantDetail(restaurant, v)
            v.findViewById<TextView>(R.id.restaurant_name).text = restaurant.name
            v.findViewById<TextView>(R.id.restaurant_note).text = "${restaurant.rating} ⭐️"
            v.findViewById<TextView>(R.id.restaurant_address).text = restaurant.adresse
            var type = ""
            restaurant.types.forEach { type += "$it    " }
            v.findViewById<TextView>(R.id.categories).text = type
            v.findViewById<Button>(R.id.add_note).setOnClickListener {
                findNavController().navigate(R.id.addNoteFragment)
            }
        }
    }

    private fun getRestaurantDetail(restaurant: Restaurant, view: View){
        val call: Call<ApiDetailResponse> = service.getRestaurantDetails(restaurant.id)
        call.enqueue(object : Callback<ApiDetailResponse?> {
            override fun onResponse(call: Call<ApiDetailResponse?>, response: Response<ApiDetailResponse?>) {
                val recyc = requireView().findViewById<RecyclerView>(R.id.recycler_view_review)
                Log.d("TAG", "${response.code()}" )
                val resource: ApiDetailResponse? = response.body()
                val detail = resource?.result!!
                recyc.adapter = ReviewAdaptateur(detail.reviews)
                //view.findViewById<TextView>(R.id.website).text = detail.website
            }

            override fun onFailure(call: Call<ApiDetailResponse?>, t: Throwable) {
                Log.d("TAG", "NOPE" )
                t.printStackTrace();
            }
        })
    }


}