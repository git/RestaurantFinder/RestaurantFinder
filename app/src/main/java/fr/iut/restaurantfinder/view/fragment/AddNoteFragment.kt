package fr.iut.restaurantfinder.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.room.Room
import fr.iut.restaurantfinder.R
import fr.iut.restaurantfinder.model.Note
import fr.iut.restaurantfinder.model.NoteDatabase
import fr.iut.restaurantfinder.viewmodel.NoteViewModel
import fr.iut.restaurantfinder.viewmodel.RestaurantViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class AddNoteFragment: Fragment(R.layout.add_note_layout) {

    private val sharedViewModel: RestaurantViewModel by activityViewModels()
    private val noteViewModel: NoteViewModel by activityViewModels()


    override fun onViewCreated(v: View, savedInstanceState: Bundle?) {
        super.onViewCreated(v, savedInstanceState)
        val restaurant = sharedViewModel.restaurant.value

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(R.id.restaurantDetailFragment)
        }

        if (restaurant != null) {
            Log.d("TAG", "${restaurant.id}" )
            v.findViewById<TextView>(R.id.restaurant_name_addnote).text = restaurant.name
            v.findViewById<TextView>(R.id.restaurant_address_addnote).text = restaurant.adresse
            var type = ""
            restaurant.types.forEach { type += "$it    " }
            v.findViewById<TextView>(R.id.categories_addnote).text = type

            v.findViewById<Button>(R.id.create_note).setOnClickListener {

                var rating = -1.0
                val ratingText = v.findViewById<EditText>(R.id.restaurant_addrating).text.toString()
                if (ratingText.isNotEmpty()) rating = ratingText.toDouble()
                val review = v.findViewById<EditText>(R.id.review_addnote).text.toString()

                if (checkValues(rating, review)){
                    val noteDatabase = NoteDatabase.getInstace(requireContext())
                    val note = Note(etoile = rating, avis = review, restaurant = restaurant)
                    GlobalScope.launch {
                        noteDatabase.noteDao().insert(note)
                    }
                    noteViewModel.setNote(note)
                    findNavController().navigate(R.id.noteFragment)
                }
                else{
                    Toast.makeText(requireContext(), getString(R.string.pb_info), Toast.LENGTH_LONG).show()
                }
            }
        }

    }

    private fun checkValues(rating: Double, review: String): Boolean = rating in 0.0..5.0 && review.isNotEmpty()

}