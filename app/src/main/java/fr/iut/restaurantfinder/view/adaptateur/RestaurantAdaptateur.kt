package fr.iut.restaurantfinder.view.adaptateur

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.*
import fr.iut.restaurantfinder.R
import fr.iut.restaurantfinder.model.Restaurant
import fr.iut.restaurantfinder.view.viewHolder.RestaurantViewHolder
import androidx.fragment.app.activityViewModels
import fr.iut.restaurantfinder.viewmodel.RestaurantViewModel


class RestaurantAdaptateur(val restaurants: List<Restaurant>, val sharedViewModel: RestaurantViewModel) : RecyclerView.Adapter<RestaurantViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RestaurantViewHolder {
        return RestaurantViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.restaurant_cellule, parent, false))
    }

    override fun onBindViewHolder(holder: RestaurantViewHolder, position: Int) {
        holder.viewItem.setOnClickListener {
            sharedViewModel.setRestaurant(restaurants[position])
            holder.viewItem.findNavController().navigate(R.id.restaurantDetailFragment)
        }
        holder.viewItem.findViewById<TextView>(R.id.nom).text = restaurants[position].name
        holder.viewItem.findViewById<TextView>(R.id.rating).text = "${restaurants[position].rating} ⭐️"
    }

    override fun getItemCount(): Int {
        return restaurants.size
    }




}