package fr.iut.restaurantfinder.view.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import fr.iut.restaurantfinder.R
import fr.iut.restaurantfinder.viewmodel.NoteViewModel

class NoteFragment: Fragment(R.layout.note_layout) {

    private val sharedViewModel: NoteViewModel by activityViewModels()

    override fun onViewCreated(v: View, savedInstanceState: Bundle?) {
        super.onViewCreated(v, savedInstanceState)
        val myNote = sharedViewModel.note.value

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner) {
            findNavController().navigate(R.id.mapFragment)
        }

        if (myNote != null) {
            var type = ""
            Log.d("TAG", "${myNote.etoile} ⭐️")
            myNote.restaurant.types.forEach { type += "$it    " }
            v.findViewById<TextView>(R.id.categories_note).text = type
            v.findViewById<TextView>(R.id.restaurant_name_note).text = myNote.restaurant.name
            v.findViewById<TextView>(R.id.restaurant_address_note).text = myNote.restaurant.adresse
            v.findViewById<TextView>(R.id.restaurant_rating).text = "${myNote.etoile} ⭐️"
            v.findViewById<TextView>(R.id.review_note).text = myNote.avis
        }
    }
}