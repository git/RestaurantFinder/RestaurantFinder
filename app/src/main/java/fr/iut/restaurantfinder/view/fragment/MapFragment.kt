package fr.iut.restaurantfinder.view.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableList
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import fr.iut.restaurantfinder.R
import fr.iut.restaurantfinder.model.NoteDatabase
import fr.iut.restaurantfinder.model.Restaurant
import fr.iut.restaurantfinder.model.mapApi.ApiResponse
import fr.iut.restaurantfinder.model.mapApi.RestaurantService
import fr.iut.restaurantfinder.model.mapApi.createRestaurantRetrofit
import fr.iut.restaurantfinder.view.adaptateur.RestaurantAdaptateur
import fr.iut.restaurantfinder.viewmodel.RestaurantViewModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.osmdroid.api.IMapController
import org.osmdroid.config.Configuration
import org.osmdroid.tileprovider.tilesource.TileSourceFactory
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.CustomZoomButtonsController
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.Marker
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.create


class MapFragment : Fragment(R.layout.map_layout) {
    private lateinit var mMap : MapView
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private var restaurants: ObservableList<Restaurant> = ObservableArrayList()
    private val sharedViewModel: RestaurantViewModel by activityViewModels()
    private lateinit var controller : IMapController
    private val service = createRestaurantRetrofit().create<RestaurantService>()
    private lateinit var markerUser: Marker

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        Configuration.getInstance().load(view.context, PreferenceManager.getDefaultSharedPreferences(view.context))

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(view.context)

        mMap = view.findViewById(R.id.laMap)
        mMap.setTileSource(TileSourceFactory.MAPNIK)
        mMap.minZoomLevel = 5.0
        mMap.zoomController.setVisibility(CustomZoomButtonsController.Visibility.NEVER)
        controller = mMap.controller
        controller.setZoom(15.5)

        val recyc = requireView().findViewById<RecyclerView>(R.id.recycler_view_map)
        recyc.layoutManager = LinearLayoutManager(requireContext())
        recyc.adapter = RestaurantAdaptateur(restaurants, sharedViewModel)

        requireView().findViewById<Button>(R.id.new_region).setOnClickListener {
            getNearRestaurants(mMap.mapCenter.latitude, mMap.mapCenter.longitude)
        }

        restaurants.addOnListChangedCallback(object :
            ObservableList.OnListChangedCallback<ObservableList<Restaurant>>(){
            override fun onChanged(sender: ObservableList<Restaurant>?) {
                updateUI(recyc)
            }

            override fun onItemRangeChanged(
                sender: ObservableList<Restaurant>?,
                positionStart: Int,
                itemCount: Int
            ) {
                updateUI(recyc)
            }

            override fun onItemRangeInserted(
                sender: ObservableList<Restaurant>?,
                positionStart: Int,
                itemCount: Int
            ) {
                updateUI(recyc)
            }

            override fun onItemRangeMoved(
                sender: ObservableList<Restaurant>?,
                fromPosition: Int,
                toPosition: Int,
                itemCount: Int
            ) {
            }

            override fun onItemRangeRemoved(
                sender: ObservableList<Restaurant>?,
                positionStart: Int,
                itemCount: Int
            ) {
            }
        })
        getLastKnownLocation()
    }

    private fun updateUI(recyc: RecyclerView){
        mMap.overlays.clear()
        recyc.adapter = RestaurantAdaptateur(restaurants, sharedViewModel)
        restaurants.forEach {
            val m = Marker(mMap)
            m.position = GeoPoint(it.geometry.location.latitude, it.geometry.location.longitude)
            m.setAnchor(Marker.ANCHOR_CENTER, Marker.ANCHOR_BOTTOM)
            m.setOnMarkerClickListener { _, _ ->
                sharedViewModel.setRestaurant(it)
                findNavController().navigate(R.id.restaurantDetailFragment)
                true
            }
            mMap.overlays.add(m)
        }
        mMap.overlays.add(markerUser)
        mMap.invalidate()
    }

    private fun getLastKnownLocation() {

        if (ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
            && ContextCompat.checkSelfPermission(
                requireActivity(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            )
            == PackageManager.PERMISSION_GRANTED
        ) {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location ->
                    if (location != null) {
                        Log.d("TAG", "TEST")
                        GlobalScope.launch {
                            getNearRestaurants(location.latitude, location.longitude)
                            Log.d("TAG", "COUCOU")
                        }
                        val geo = GeoPoint(location.latitude, location.longitude)
                        controller.setCenter(geo)
                        controller.animateTo(geo)
                        markerUser = Marker(mMap)
                        markerUser.position=geo
                        markerUser.icon = requireContext().resources.getDrawable(R.drawable.marker_user)
                        markerUser.setAnchor(Marker.ANCHOR_TOP, Marker.ANCHOR_BOTTOM)
                        mMap.overlays.add(markerUser)
                    }
                }
        } else {
            requestPermissions(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                100
            )
        }
    }

    @SuppressLint("MissingPermission")
    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(
            requestCode, permissions, grantResults
        )
        // Check condition
        if (requestCode == 100 && grantResults.isNotEmpty()
            && (grantResults[0] + grantResults[1]
                    == PackageManager.PERMISSION_GRANTED)
        ) {
            fusedLocationClient.lastLocation
                .addOnSuccessListener { location ->
                    if (location != null) {
                        GlobalScope.launch{
                            getNearRestaurants(location.latitude, location.longitude)
                            Log.d("TAG", "COUCOU")
                        }
                        val geo = GeoPoint(location.latitude, location.longitude)
                        controller.setCenter(geo)
                        controller.animateTo(geo)
                        markerUser = Marker(mMap)
                        markerUser.position=geo
                        markerUser.icon = requireContext().resources.getDrawable(R.drawable.marker_user)
                        markerUser.setAnchor(Marker.ANCHOR_TOP, Marker.ANCHOR_BOTTOM)
                        mMap.overlays.add(markerUser)
                    }
                }
        } else {
            Toast
                .makeText(
                    activity,
                    "Permission denied",
                    Toast.LENGTH_SHORT
                )
                .show()
        }
    }




    private fun getNearRestaurants(latitude: Double, longitude: Double) {
        val call: Call<ApiResponse> = service.getRestaurants("$latitude%2C$longitude")
        call.enqueue(object : Callback<ApiResponse?> {
            override fun onResponse(call: Call<ApiResponse?>?, response: Response<ApiResponse?>) {
                Log.d("TAG", "${response.code()}" )
                val resource: ApiResponse? = response.body()
                restaurants.clear()
                restaurants.addAll(resource?.results!!)
            }

            override fun onFailure(call: Call<ApiResponse?>, t: Throwable?) {
                call.cancel()
                t?.printStackTrace();
                Toast.makeText(requireContext(), "Veuillez vérifier votre connexion internet", Toast.LENGTH_LONG).show()
            }
        })
    }


}